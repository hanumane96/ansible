terraform {
  backend "s3" {
    bucket = "host.preetz.fun"
    region = "us-east-1"
    key    = "terraform.tfstate"
  }
}

provider "aws" {
  region = var.region
}

data "aws_security_group" "my_sg" {
  name = "my-sg"
}

resource "aws_instance" "my_instance" {
  ami             = var.ami
  instance_type   = var.instance_type
  key_name        = var.key_name
  tags            = var.tags

  network_interface {
    network_interface_id = aws_network_interface.my-nic.id
    device_index         = 0 
  }
}

resource "aws_network_interface" "my-nic" {
  subnet_id          = "subnet-0de05e2de9803e1fe"
  private_ips        = ["172.31.0.11"]
  security_groups = data.aws_security_group.id
}

variable "region" {
  default = "sa-east-1"
}

variable "ami" {
  default = "ami-0d0580fca308ea168"
}

variable "instance_type" {
  default = "t2.micro"
}

variable "key_name" {
  default = "sao-key"
}

variable "tags" {
  type    = map
  default = {
    env  = "dev"
    Name = "my-instance"
  }
}

#variable "sg_ids" {
 # type    = list
  #default = ["sg-0999bf19b45b6d56b"]
#}
