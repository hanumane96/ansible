terraform {
  backend "s3" {
    bucket = "host.preetz.fun"
    region = "us-east-1"
    key    = "terraform.tfstate123"
  }
}

provider "aws" {
  region = var.region
}

data "aws_instance" "instance-2" {
    instance_id = "i-062b6d8c392092f99"
}


resource "aws_instance" "my_instance1" {
  ami                    = data.aws_instance.instance-2.ami
  instance_type          = data.aws_instance.instance-2.instance_type
  key_name               = data.aws_instance.instance-2.key_name
  tags                   = var.tags
  vpc_security_group_ids = data.aws_instance.instance-2.vpc_security_group_ids
}

variable "region" {
  default = "sa-east-1"
}

#variable "ami" {
  #default = "ami-0d0580fca308ea168"
#}

#variable "instance_type" {
 # default = "t2.micro"
#}

#variable "key_name" {
#  default = "sao-key"
#}

variable "tags" {
  type    = map
  default = {
    env  = "dev"
    Name = "my-instance1"
  }
}
#variable "sg_ids" {
 # type    = list
  #default = ["sg-0999bf19b45b6d56b"]
#}