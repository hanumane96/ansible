variable "terraform-backend" {
    default = "host.preetz.fun"
}

variable "region" {
    default "sa-east-1"
}

variable "cidr_block" {
    default = "10.10.0.0/20"
}

variable "vpc_name" {
    default = "cbz-vpc"
}

variable "env" {
    default = "dev"
}
variable "pri_subnet-cidr" {
    default = "10.10.
}
