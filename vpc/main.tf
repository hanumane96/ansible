terraform 
    backend "s3" {
        bucket = var.terraform-backend
        region = "us-east-1"
        key = "tf1"
    }

provider "aws" {
    region = var.region
}

resource "aws_vpc" "my_vpc" {
  cidr_block = var.cidr_block
  tags {
    env = var.env
    Name = var.vpc_name
  }
}

resource "aws_subnet" "pri_subnet" {
    vpc_id = aws_vpc.my_vpc.id
    cidr_block = var.pri_subnet-cidr
}
